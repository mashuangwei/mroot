/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.PropertySource
import org.springframework.scheduling.annotation.EnableAsync


/**
 * 入口
 *
 * @author ErYang
 */
@SpringBootApplication(scanBasePackages = ["wang.encoding.mroot"])
@EnableAsync
@PropertySource("classpath:common.properties", "classpath:ueditor.properties")
@EnableCaching
class MRootAdminApplication : SpringBootServletInitializer() {

    /**
     * 不使用 Spring Boot 内置的 tomcat
     */
//    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
//        return application.sources(MRootAdminApplication::class.java)
//    }

    // -------------------------------------------------------------------------------------------------

    companion object {
        /**
         * logger 日志
         */
        private val logger: Logger = LoggerFactory.getLogger(MRootAdminApplication::class.java)

        @JvmStatic
        fun main(args: Array<String>) {
            val ctx: ConfigurableApplicationContext = SpringApplication.run(MRootAdminApplication::class.java, *args)
            // 得到当前模式
            val activeProfiles: Array<String> = ctx.environment.activeProfiles
            for (profile: String in activeProfiles) {
                if (logger.isDebugEnabled) {
                    logger.debug(">>>>>>>>服务启动成功，当前模式[$profile]<<<<<<<<")
                }
            }
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

}


// -------------------------------------------------------------------------------------------------

//    @Bean
//    public EmbeddedServletContainerCustomizer containerCustomizer() {
//
//        return new EmbeddedServletContainerCustomizer() {
//            @Override
//            public void customize(ConfigurableEmbeddedServletContainer container) {
//
//                // ERROR PAGE
//                ErrorPage error401Page = new ErrorPage(HttpStatus.UNAUTHORIZED, "/error/401.html");
//                ErrorPage error403Page = new ErrorPage(HttpStatus.FORBIDDEN, "/error/403.html");
//                ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/error/404.html");
//                ErrorPage error405Page = new ErrorPage(HttpStatus.METHOD_NOT_ALLOWED, "/error/405.html");
//                ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/error/500.html");
//                container.addErrorPages(error401Page, error403Page, error404Page,error405Page, error500Page);
//            }
//        };
//    }


// -----------------------------------------------------------------------------------------------------

// End MRootAdminApplication class

/* End of file MRootAdminApplication.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/MRootAdminApplication.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
