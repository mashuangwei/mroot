/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.cms.article


import com.alibaba.fastjson.JSONObject
import com.baomidou.mybatisplus.plugins.Page
import org.apache.shiro.authz.annotation.RequiresPermissions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import wang.encoding.mroot.admin.common.controller.BaseAdminController
import wang.encoding.mroot.common.annotation.FormToken
import wang.encoding.mroot.common.annotation.RequestLogAnnotation
import wang.encoding.mroot.common.business.ResultData
import wang.encoding.mroot.common.constant.RequestLogConstant
import wang.encoding.mroot.common.exception.ControllerException
import wang.encoding.mroot.common.util.FileUtil
import wang.encoding.mroot.common.util.ImageUtil
import wang.encoding.mroot.model.entity.cms.Article
import wang.encoding.mroot.model.entity.cms.ArticleContent
import wang.encoding.mroot.model.entity.cms.Category
import wang.encoding.mroot.model.enums.BooleEnum
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.cms.ArticleContentService
import wang.encoding.mroot.service.cms.ArticleService
import wang.encoding.mroot.service.cms.CategoryService
import java.io.*
import java.math.BigInteger
import java.util.*
import javax.servlet.http.HttpServletRequest


/**
 * 后台 文章 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping(value = ["/article"])
class ArticleController : BaseAdminController() {


    @Autowired
    private lateinit var articleService: ArticleService

    @Autowired
    private lateinit var categoryService: CategoryService

    @Autowired
    private lateinit var articleContentService: ArticleContentService


    companion object {

        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(ArticleController::class.java)

        /**
         * 模块
         */
        private const val MODULE_NAME: String = "/article"
        /**
         * 视图目录
         */
        private const val VIEW_PATH: String = "/cms/article"
        /**
         * 对象名称
         */
        private const val VIEW_MODEL_NAME: String = "article"
        /**
         * 首页
         */
        private const val INDEX: String = "/index"
        private const val INDEX_URL: String = MODULE_NAME + INDEX
        private const val INDEX_VIEW: String = VIEW_PATH + INDEX
        /**
         * 新增
         */
        private const val ADD: String = "/add"
        private const val ADD_URL: String = MODULE_NAME + ADD
        private const val ADD_VIEW: String = VIEW_PATH + ADD

        private const val UPLOAD: String = "/uploadCover"
        private const val UPLOAD_URL: String = MODULE_NAME + UPLOAD

        /**
         * 保存
         */
        private const val SAVE: String = "/save"
        private const val SAVE_URL: String = MODULE_NAME + SAVE
        /**
         * 修改
         */
        private const val EDIT: String = "/edit"
        private const val EDIT_URL: String = MODULE_NAME + EDIT
        private const val EDIT_VIEW: String = VIEW_PATH + EDIT
        /**
         * 更新
         */
        private const val UPDATE: String = "/update"
        private const val UPDATE_URL: String = MODULE_NAME + UPDATE
        /**
         * 查看
         */
        private const val VIEW: String = "/view"
        private const val VIEW_URL: String = MODULE_NAME + VIEW
        private const val VIEW_VIEW: String = VIEW_PATH + VIEW
        /**
         * 删除
         */
        private const val DELETE: String = "/delete"
        private const val DELETE_URL: String = MODULE_NAME + DELETE
        private const val DELETE_BATCH: String = "/deleteBatch"
        private const val DELETE_BATCH_URL: String = MODULE_NAME + DELETE_BATCH

        /**
         * 回收站
         */
        private const val RECYCLE_BIN_INDEX: String = "/recycleBin"
        private const val RECYCLE_BIN_INDEX_URL: String = MODULE_NAME + RECYCLE_BIN_INDEX
        private const val RECYCLE_BIN_INDEX_VIEW: String = VIEW_PATH + RECYCLE_BIN_INDEX
        /**
         * 恢复
         */
        private const val RECOVER: String = "/recover"
        private const val RECOVER_URL: String = MODULE_NAME + RECOVER

        private const val RECOVER_BATCH: String = "/recoverBatch"
        private const val RECOVER_BATCH_URL: String = MODULE_NAME + RECOVER_BATCH

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [INDEX_URL])
    @RequestMapping(INDEX)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_INDEX)
    @Throws(ControllerException::class)
    fun index(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(INDEX_VIEW))
        super.initViewTitleAndModelUrl(INDEX_URL, MODULE_NAME, request)

        val article = Article()
        val title: String? = request.getParameter("title")
        if (null != title && title.isNotBlank()) {
            article.title = title
            modelAndView.addObject("title", title)
        }
        article.status = StatusEnum.NORMAL.key
        val page: Page<Article> = articleService.list2page(super.initPage(request), article, Article.ID, false)!!
        modelAndView.addObject(VIEW_PAGE_NAME, page)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [ADD_URL])
    @RequestMapping(ADD)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_ADD)
    @FormToken(init = true)
    @Throws(ControllerException::class)
    fun add(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(ADD_VIEW))
        super.initViewTitleAndModelUrl(ADD_URL, MODULE_NAME, request)

        // 最大排序值
        val maxSort: Int = articleService.getMax2Sort() + 1
        modelAndView.addObject("maxSort", maxSort)

        this.getCategory2Tree(request)

        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param request HttpServletRequest
     * @param article Article
     * @param articleContent ArticleContent
     * @return Any
     */
    @RequiresPermissions(value = [SAVE_URL])
    @RequestMapping(SAVE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_SAVE)
    @FormToken(remove = true)
    @Throws(ControllerException::class)
    fun save(request: HttpServletRequest, article: Article, articleContent: ArticleContent): Any {

        if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()

            // 创建 Article 对象
            val saveArticle: Article = this.initAddData(request, article)

            // 是否前台可见
            val showStr: String? = request.getParameter("showStr")
            when {
                showStr.equals(configProperties.bootstrapSwitchEnabled) -> saveArticle.show = BooleEnum.YES.key
                null == showStr -> saveArticle.show = BooleEnum.NO.key
                else -> saveArticle.show = BooleEnum.NO.key
            }

            // Hibernate Validation  验证数据
            val validationResult: String? = articleService.validationArticle(saveArticle)
            if (null != validationResult && validationResult.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }

            // 创建 ArticleContent 对象
            val saveArticleContent: ArticleContent = this.initAddData2Content(request, articleContent)

            // Hibernate Validation  验证数据
            val validationResult2Content: String? = articleContentService.validationArticleContent(saveArticleContent)
            if (null != validationResult2Content && validationResult2Content.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult2Content)
            }

            // 验证数据唯一性
            val flag: Boolean = this.validationAddData(saveArticle, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }

            // 新增用户 id
            val id: BigInteger? = articleService.saveBackId(saveArticle)
            saveArticle.id = id

            if (null != id && BigInteger.ZERO < id) {
                // 异步新增文章内容
                businessAsyncTask.addArticleContent(saveArticle, saveArticleContent)
            }
            return super.initSaveJSONObject(id)
        } else {
            return super.initErrorRedirectUrl()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 上传图片封面
     * @param request HttpServletRequest
     * @param file MultipartFile
     * @return JSONObject
     */
    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    @RequiresPermissions(value = [UPLOAD_URL])
    @RequestMapping(UPLOAD)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_RECOVER)
    @Throws(ControllerException::class)
    fun uploadCover(request: HttpServletRequest, @RequestParam("file") file: MultipartFile): Any? {
        val failResult: ResultData = ResultData.fail()
        if (null != file.originalFilename && file.originalFilename.isNotBlank()) {
            // 图片类型
            val type: String = FileUtil.getSuffixByFilename(file.originalFilename)
            if (ImageUtil.isNotImage(type)) {
                return super.initErrorJSONObject(failResult, "message.upload.image.file.error")
            }
            // 文件的大小
            if (FileUtil.gt2Mb(file.size)) {
                return super.initErrorJSONObject(failResult, "message.upload.file.over.size")
            }

            // 保存图片的路径
            val savePath: String = FileUtil.createUploadImgPath2Article(uploadResourcesPath)
            // 上传图片
            val uploadFile: File = ImageUtil.uploadImageByInputStream(file.inputStream, savePath, file.originalFilename)
            val url: String = ARTICLE_UPLOAD_PATH_URL + DATE_FORMAT.format(Date()) + "/" + uploadFile.name

            val resultData: ResultData = ResultData.ok(configProperties.messageName, "$contextPath/$url")
            return resultData.toFastJson()
        } else {
            return super.initReturnErrorJSONObject()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(value = [EDIT_URL])
    @RequestMapping("$EDIT/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_EDIT)
    @FormToken(init = true)
    @Throws(ControllerException::class)
    fun edit(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(EDIT_VIEW))


        super.initViewTitleAndModelUrl(EDIT_URL, MODULE_NAME, request)
        val idValue: BigInteger? = super.getId(id)
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        // 数据真实性
        val article: Article? = articleService.getById(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (StatusEnum.DELETE.key == article!!.status) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        modelAndView.addObject(VIEW_MODEL_NAME, article)

        val articleContent: ArticleContent = articleContentService.getById(idValue)!!
        modelAndView.addObject("articleContent", articleContent)

        this.getCategory2Tree(request)


        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @param request HttpServletRequest
     * @param article Article
     * @param articleContent ArticleContent
     * @return ModelAndView
     */
    @RequiresPermissions(value = [UPDATE_URL])
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_UPDATE)
    @FormToken(remove = true)
    @Throws(ControllerException::class)
    fun update(request: HttpServletRequest, article: Article, articleContent: ArticleContent): Any {
        if (httpRequestUtil.isAjaxRequest(request)) {
            val failResult: ResultData = ResultData.fail()
            // 验证数据
            val idValue: BigInteger? = super.getId(request)
            if (null == idValue || BigInteger.ZERO > idValue) {
                return super.initErrorCheckJSONObject(failResult)
            }
            // 数据真实性
            val articleBefore: Article = articleService.getById(idValue)
                    ?: return super.initErrorCheckJSONObject(failResult)
            if (StatusEnum.DELETE.key == articleBefore.status) {
                return super.initErrorCheckJSONObject(failResult)
            }

            // 创建 Article 对象
            var editArticle: Article = Article.copy2New(article)
            editArticle.id = articleBefore.id
            val statusStr: String? = super.getStatusStr(request)
            when {
                statusStr.equals(configProperties.bootstrapSwitchEnabled) -> editArticle.status = StatusEnum.NORMAL.key
                null == statusStr -> editArticle.status = StatusEnum.DISABLE.key
                else -> editArticle.status = StatusEnum.DISABLE.key
            }
            // 是否前台可见
            val showStr: String? = request.getParameter("showStr")
            when {
                showStr.equals(configProperties.bootstrapSwitchEnabled) -> editArticle.show = BooleEnum.YES.key
                null == showStr -> editArticle.show = BooleEnum.NO.key
                else -> editArticle.show = BooleEnum.NO.key
            }
            editArticle = this.initEditData(editArticle)

            // Hibernate Validation 验证数据
            val validationResult: String? = articleService.validationArticle(editArticle)
            if (null != validationResult && validationResult.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult)
            }

            // 数据真实性
            val articleContentBefore: ArticleContent = articleContentService.getById(idValue)
                    ?: return super.initErrorCheckJSONObject(failResult)
            if (StatusEnum.DELETE.key == articleContentBefore.status) {
                return super.initErrorCheckJSONObject(failResult)
            }

            // 创建 ArticleContent 对象
            val editArticleContent: ArticleContent = this.initEditData2Content(articleContent)
            editArticleContent.id = articleContentBefore.id
            when {
                statusStr.equals(configProperties.bootstrapSwitchEnabled) ->
                    editArticleContent.status = StatusEnum.NORMAL.key
                null == statusStr -> editArticleContent.status = StatusEnum.DISABLE.key
                else -> editArticleContent.status = StatusEnum.DISABLE.key
            }

            // Hibernate Validation  验证数据
            val validationResult2Content: String? = articleContentService.validationArticleContent(editArticleContent)
            if (null != validationResult2Content && validationResult2Content.isNotBlank()) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult2Content)
            }

            // 验证数据唯一性
            val flag: Boolean = this.validationEditData(article,
                    articleBefore, failResult)
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult)
            }

            // 修改 Article id
            val id: BigInteger? = articleService.updateBackId(editArticle)

            if (null != id && BigInteger.ZERO < id) {
                // 异步修改文章内容
                businessAsyncTask.editArticleContent(editArticle, editArticleContent)
            }
            return super.initUpdateJSONObject(id)
        } else {
            return super.initErrorRedirectUrl()
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id String
     * @param request HttpServletRequest
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(value = [VIEW_URL])
    @RequestMapping("$VIEW/{id}")
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_VIEW)
    @Throws(ControllerException::class)
    fun view(@PathVariable(ID_NAME) id: String, request: HttpServletRequest,
             redirectAttributes: RedirectAttributes): ModelAndView {
        val modelAndView = ModelAndView(super.initView(VIEW_VIEW))
        super.initViewTitleAndModelUrl(VIEW_URL, MODULE_NAME, request)

        val idValue: BigInteger? = super.getId(id)
        // 验证数据
        if (null == idValue || BigInteger.ZERO > idValue) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        }
        // 数据真实性
        val article: Article? = articleService.getById(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (null != article) {
            modelAndView.addObject(VIEW_MODEL_NAME, article)
        }
        val articleContent: ArticleContent? = articleContentService.getById(idValue)
                ?: return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL)
        if (null != articleContent) {
            modelAndView.addObject("articleContent", articleContent)
        }

        // 设置上个请求地址
        super.initRefererUrl(request)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [DELETE_URL])
    @RequestMapping("$DELETE/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_DELETE)
    @Throws(ControllerException::class)
    fun delete(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val article: Article? = articleService.getById(idValue!!)
            if (null == article) {
                super.initErrorCheckJSONObject()
            }
            // 删除 Article id
            val backId: BigInteger? = articleService.removeBackId(article!!.id!!)

            if (null != backId && BigInteger.ZERO < backId) {
                // 异步删除文章内容
                businessAsyncTask.removeArticleContent(article)

            }
            return super.initDeleteJSONObject(backId)
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     * @param request HttpServletRequest
     * @return JSONObject
     */
    @RequiresPermissions(value = [DELETE_BATCH_URL])
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_DELETE_BATCH)
    @Throws(ControllerException::class)
    fun deleteBatch(request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idArray: ArrayList
            <BigInteger>? = super.getIdArray(request)
            // 验证数据
            if (null == idArray || idArray.isEmpty()) {
                super.initErrorCheckJSONObject()
            }
            var flag = false
            if (null != idArray) {
                flag = articleService.removeBatch2UpdateStatus(idArray)!!
            }
            return if (flag) {
                // 异步删除文章内容
                businessAsyncTask.removeBatchArticleContent2UpdateStatus(idArray!!)

                super.initDeleteJSONObject(BigInteger.ONE)
            } else {
                super.initDeleteJSONObject(BigInteger.ZERO)
            }
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 回收站页面
     *
     * @param request HttpServletRequest
     * @return ModelAndView
     */
    @RequiresPermissions(value = [RECYCLE_BIN_INDEX_URL])
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_RECYCLE_BIN_INDEX)
    @Throws(ControllerException::class)
    fun recycleBin(request: HttpServletRequest): ModelAndView {
        val modelAndView = ModelAndView(super.initView(RECYCLE_BIN_INDEX_VIEW))
        super.initViewTitleAndModelUrl(RECYCLE_BIN_INDEX_URL, MODULE_NAME, request)

        val article = Article()
        val title: String? = request.getParameter("title")
        if (null != title && title.isNotBlank()) {
            article.title = title
            modelAndView.addObject("title", title)
        }
        article.status = StatusEnum.DELETE.key
        val page: Page<Article> = articleService.list2page(super.initPage(request), article, Article.ID, false)!!
        modelAndView.addObject(VIEW_PAGE_NAME, page)
        return modelAndView
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复
     * @param request HttpServletRequest
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(value = [RECOVER_URL])
    @RequestMapping("$RECOVER/{id}")
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_RECOVER)
    @Throws(ControllerException::class)
    fun recover(@PathVariable(ID_NAME) id: String, request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idValue: BigInteger? = super.getId(id)
            // 验证数据
            if (null == idValue || BigInteger.ZERO > idValue) {
                super.initErrorCheckJSONObject()
            }
            val article: Article? = articleService.getById(idValue!!)
            if (null == article) {
                super.initErrorCheckJSONObject()
            }
            // 恢复 Article id
            val backId: BigInteger? = articleService.recoverBackId(article!!.id!!)

            if (null != backId && BigInteger.ZERO < backId) {
                // 异步恢复文章内容
                businessAsyncTask.recoverArticleContent(article)
            }
            return super.initRecoverJSONObject(backId)
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量恢复
     * @param request HttpServletRequest
     * @return JSONObject
     */
    @RequiresPermissions(value = [RECOVER_BATCH_URL])
    @RequestMapping(RECOVER_BATCH)
    @ResponseBody
    @RequestLogAnnotation(module = RequestLogConstant.ADMIN_MODULE,
            title = RequestLogConstant.ADMIN_MODULE_CMS_ARTICLE_RECOVER_BATCH)
    @Throws(ControllerException::class)
    fun recoverBatch(request: HttpServletRequest): JSONObject? {
        if (super.isAjaxRequest(request)) {
            val idArray: ArrayList
            <BigInteger>? = super.getIdArray(request)
            // 验证数据
            if (null == idArray || idArray.isEmpty()) {
                super.initErrorCheckJSONObject()
            }
            var flag = false
            if (null != idArray) {
                flag = articleService.recoverBatch2UpdateStatus(idArray)!!
            }
            return if (flag) {
                // 异步恢复文章内容
                businessAsyncTask.recoverBatchArticleContent2UpdateStatus(idArray!!)

                super.initRecoverJSONObject(BigInteger.ONE)
            } else {
                super.initRecoverJSONObject(BigInteger.ZERO)
            }
        }
        return super.initReturnErrorJSONObject()
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param request HttpServletRequest
     * @param article         Article
     * @return Article
     */
    private fun initAddData(request: HttpServletRequest, article: Article): Article {
        val addArticle: Article = Article.copy2New(article)
        // ip
        addArticle.gmtCreateIp = super.getIp(request)
        // 创建 Article 对象
        return articleService.initSaveArticle(addArticle)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param request HttpServletRequest
     * @param articleContent         ArticleContent
     * @return ArticleContent
     */
    private fun initAddData2Content(request: HttpServletRequest, articleContent: ArticleContent): ArticleContent {
        val addArticleContent: ArticleContent = ArticleContent.copy2New(articleContent)
        addArticleContent.contentHtml = articleContent.content
        // ip
        addArticleContent.gmtCreateIp = super.getIp(request)
        // 创建 Article 对象
        return articleContentService.initSaveArticleContent(addArticleContent)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param article         Article
     * @return Article
     */
    private fun initEditData(article: Article): Article {
        val editArticle: Article = article
        // 创建 Article 对象
        return articleService.initEditArticle(editArticle)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param articleContent         ArticleContent
     * @return ArticleContent
     */
    private fun initEditData2Content(articleContent: ArticleContent): ArticleContent {
        val editArticleContent: ArticleContent = articleContent
        editArticleContent.content = articleContent.content
        editArticleContent.contentHtml = articleContent.content
        // 创建 ArticleContent 对象
        return articleContentService.initEditArticleContent(editArticleContent)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param article         Article
     * @param failResult ResultData
     * @return Boolean true(通过)/false(未通过)
     */
    private fun validationAddData(article: Article,
                                  failResult: ResultData):
            Boolean {
        val message: String
        // 是否存在
        val titleExist: Article? = articleService.getByTitle(article.title!!.trim().toLowerCase())
        if (null != titleExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_TITLE_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }
        return true
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param article         Article
     * @param failResult ResultData
     * @return Boolean true(通过)/false(未通过)
     */
    private fun validationEditData(newArticle: Article, article: Article,
                                   failResult: ResultData): Boolean {
        val message: String
        val titleExist: Boolean = articleService.propertyUnique(Article.TITLE, newArticle
                .title!!.trim().toLowerCase(), article.title!!.toLowerCase())
        if (!titleExist) {
            message = localeMessageSourceConfiguration.getMessage(BaseAdminController.MESSAGE_TITLE_EXIST_NAME)
            failResult.setFail()[configProperties.messageName] = message
            return false
        }
        return true
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到文章分类(小于3级)
     */
    private fun getCategory2Tree(request: HttpServletRequest) {
        // 权限 tree 数据
        val list: List<Category>? = categoryService.listTypeLt3()
        if (null != list) {
            request.setAttribute("treeCategory", list)
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleController class

/* End of file ArticleController.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/controller/cms/article/ArticleController.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// | ErYang出品 属于小极品 O(∩_∩)O~~ 共同学习 共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
