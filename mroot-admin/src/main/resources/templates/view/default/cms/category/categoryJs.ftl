<script>

    /**
     * 文章分类提示信息
     */
    var CategoryValidation = function () {


        var pid_range = '${I18N("jquery.validation.cms.category.pid.range")}';

        var type_range = '${I18N("jquery.validation.type.range")}';

        var name_pattern = '${I18N("jquery.validation.name.pattern")}';

        var title_pattern = '${I18N("jquery.validation.title.pattern")}';

        var audit_range = '${I18N("jquery.validation.cms.category.audit.range")}';

        var allow_range = '${I18N("jquery.validation.cms.category.allow.range")}';

        var show_range = '${I18N("jquery.validation.cms.category.show.range")}';

        var sort_range = '${I18N("jquery.validation.sort.range")}';

        var status_range = '${I18N("jquery.validation.status.range")}';

        var remark_pattern = '${I18N("jquery.validation.remark.pattern")}';


        return {

            getPidRange: function () {
                return pid_range;
            },

            getTypeRange: function () {
                return type_range;
            },

            getNamePattern: function () {
                return name_pattern;
            },

            getTitlePattern: function () {
                return title_pattern;
            },

            getauditRange: function () {
                return audit_range;
            },

            getallowRange: function () {
                return allow_range;
            },

            getshowRange: function () {
                return show_range;
            },

            getSortRange: function () {
                return sort_range;
            },

            getStatusRange: function () {
                return status_range;
            },

            getRemarkPattern: function () {
                return remark_pattern;
            }

            // -------------------------------------------------------------------------------------------------

        }
    }();

    // -------------------------------------------------------------------------------------------------

</script>
