<#-- /* 主要部分 */ -->
${r'<@OVERRIDE name="MAIN_CONTENT">'}

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                ${r"${"}I18N("message.form.head.title")${r"}"}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right">
        <div class="m-portlet__body">
<#list generateModels as var>

<#if "id" == var.camelCaseName >
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${r"${"}${classFirstLowerCaseName}.${var.camelCaseName}${r"}"}</span>
                </div>
            </div>
<#elseif "name" == var.camelCaseName>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.${var.camelCaseName}${r'></@defaultStr>'}</span>
                </div>
            </div>
<#elseif "title" == var.camelCaseName>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.${var.camelCaseName}${r'></@defaultStr>'}</span>
                </div>
            </div>
<#elseif "status" == var.camelCaseName>
         <div class="form-group m-form__group row">
             <label class="col-form-label col-lg-3 col-sm-12">
                 ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
             </label>
             <div class="col-lg-4 col-md-9 col-sm-12">
                 <span class="m-form__control-static">${r'<@statusStrTable '} ${classFirstLowerCaseName}.status>${r'</@statusStrTable>'}</span>
             </div>
         </div>
            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
<#elseif "sort" == var.camelCaseName >
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.${var.camelCaseName}${r'></@defaultStr>'}</span>
                </div>
            </div>
<#elseif "remark" == var.camelCaseName >
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.${var.camelCaseName}${r'></@defaultStr>'}</span>
                </div>
            </div>
<#elseif "gmtCreate" == var.camelCaseName >
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${r'<@dateFormat '} ${classFirstLowerCaseName}.gmtCreate${r'></@dateFormat>'}</span>
                </div>
            </div>
<#elseif "gmtCreateIp" == var.camelCaseName >
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.${var.camelCaseName}${r'></@defaultStr>'}</span>
                </div>
            </div>
<#elseif "gmtModified" == var.camelCaseName >
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${r'<@dateFormat '} ${classFirstLowerCaseName}.gmtModified${r'></@dateFormat>'}</span>
                </div>
            </div>
<#else>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">
                    ${r"${"}I18N("message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}")${r"}"}
                </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <span class="m-form__control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.${var.camelCaseName}${r'></@defaultStr>'}</span>
                </div>
            </div>
</#if>
</#list>

        </div>

    <#-- 提交按钮 -->
    ${r'<@viewFormOperate></@viewFormOperate>'}

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->

${r'</@OVERRIDE>'}
${r'<#include "/default/scriptPlugin/form.ftl">'}
${r'<@OVERRIDE name="CUSTOM_SCRIPT">'}
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${r"${"}navIndex${r"}"}');
    });
</script>
<#-- /* /.页面级别script结束 */ -->
${r'</@OVERRIDE>'}

${r'<@EXTENDS name="/default/common/base.ftl"/>'}
