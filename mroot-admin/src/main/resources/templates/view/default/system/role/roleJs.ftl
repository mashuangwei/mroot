<script>

    /**
     * 角色提示信息
     */
    var RoleValidation = function () {

        var name_pattern = '${I18N("jquery.validation.name.pattern")}';
        var title_pattern = '${I18N("jquery.validation.title.pattern")}';
        var sort_range = '${I18N("jquery.validation.sort.range")}';
        var remark_pattern = '${I18N("jquery.validation.remark.pattern")}';

        return {

            getNamePattern: function () {
                return name_pattern;
            },

            getTitlePattern: function () {
                return title_pattern;
            },

            getSortRange: function () {
                return sort_range;
            },

            getRemarkPattern: function () {
                return remark_pattern;
            }

            // -------------------------------------------------------------------------------------------------

        }
    }();

    // -------------------------------------------------------------------------------------------------

</script>
