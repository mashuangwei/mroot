/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.controller


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.InitBinder
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.ModelAndView
import wang.encoding.mroot.common.business.Profile
import wang.encoding.mroot.common.business.ResultData
import wang.encoding.mroot.common.config.LocaleMessageSourceConfiguration
import wang.encoding.mroot.common.constant.ShareConst
import wang.encoding.mroot.common.exception.BaseException
import wang.encoding.mroot.common.util.HttpRequestUtil
import javax.servlet.http.HttpServletRequest


/**
 * controller 增强器
 *
 * @author ErYang
 */
@ControllerAdvice
class BaseControllerAdvice {

    companion object {

        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(BaseControllerAdvice::class.java)

        /**
         * 默认视图目录
         */
        private const val DEFAULT_VIEW: String = "/default"

        /**
         * 错误视图
         */
        private const val ERROR_EXCEPTION_VIEW: String = "$DEFAULT_VIEW/error/500"

        /**
         * 错误视图 开发环境
         */
        private const val DEV_ERROR_EXCEPTION_VIEW: String = "$DEFAULT_VIEW/error/5002Dev"

    }

    // -------------------------------------------------------------------------------------------------

    @Autowired
    private lateinit var localeMessageSourceConfiguration: LocaleMessageSourceConfiguration

    @Autowired
    private lateinit var shareConst: ShareConst

    @Autowired
    private lateinit var httpRequestUtil: HttpRequestUtil

    @Autowired
    private lateinit var profile: Profile

    /**
     * 应用到所有 @RequestMapping 注解方法 在其执行之前初始化数据绑定器
     *
     * @param binder WebDataBinder
     */
    @InitBinder
    fun initBinder(binder: WebDataBinder) {
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 拦截捕捉自定义异常 BaseException.class
     *
     * @param request HttpServletRequest
     * @param exception BaseException
     * @return Any
     */
    @ResponseBody
    @ExceptionHandler(value = [(BaseException::class)])
    fun baseExceptionHandler(request: HttpServletRequest, exception: BaseException): Any {
        if (logger.isErrorEnabled) {
            if (null != exception.message) {
                logger.error(exception.message, exception.cause)
            } else {
                logger.error(">>>>>>>>得到抛出的异常信息<<<<<<<<", exception.cause)
            }
        }
        // ajax 请求
        return if (httpRequestUtil.isAjaxRequest(request)) {
            val result: ResultData = ResultData.error()
            result.setError()["message"] = localeMessageSourceConfiguration
                    .getMessage("message.exception.info")
            result.toFastJson()
        } else {

            // 跳转页面
            val modelAndView = ModelAndView()

            // 开发环境
            if (profile.devProfile()) {
                modelAndView.viewName = DEV_ERROR_EXCEPTION_VIEW
                if (null != exception.message && exception.message!!.isNotBlank()) {
                    modelAndView.addObject(shareConst.exceptionCode, exception.message!!)
                }
                if (null != exception.cause) {
                    modelAndView.addObject(shareConst.exceptionMessage, exception.cause!!)
                }
            } else {
                modelAndView.viewName = ERROR_EXCEPTION_VIEW
            }

            val url: String? = request.getHeader("Referer")
            if (null != url) {
                modelAndView.addObject("refererUrl", url)
            }
            modelAndView
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseControllerAdvice class

/* End of file BaseControllerAdvice.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/controller/BaseControllerAdvice.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
