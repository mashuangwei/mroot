/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.EnableCaching
import org.springframework.stereotype.Component
import wang.encoding.mroot.common.config.LocaleMessageSourceConfiguration
import java.net.InetAddress
import java.net.UnknownHostException
import javax.servlet.http.HttpServletRequest


/**
 * request工具类
 *
 * @author ErYang
 */
@Component
@EnableCaching
class HttpRequestUtil {

    @Autowired
    protected lateinit var localeMessageSourceConfiguration: LocaleMessageSourceConfiguration

    companion object {

        /**
         * ajax标识
         */
        private const val AJAX_HEADER = "x-requested-with"

        /**
         * ajax值
         */
        private const val AJAX_HEADER_VALUE = "XMLHttpRequest"

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否ajax请求
     *
     * @param request request
     * @return Boolean
     */
    fun isAjaxRequest(request: HttpServletRequest): Boolean {
        // 如果是ajax请求响应头会有x-requested-with
        return null != request.getHeader(AJAX_HEADER)
                && request.getHeader(AJAX_HEADER).equals(AJAX_HEADER_VALUE, ignoreCase = true)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到用户的 ip
     *
     * @param request HttpServletRequest
     * @return String ip地址
     */
    fun getIp(request: HttpServletRequest?): String {
        var ip: String?
        if (null == request) {
            ip = localeMessageSourceConfiguration.getMessage("message.exception.data")
        } else {
            try {
                ip = request.getHeader("x-forwarded-for")
                val unknown = "unknown"
                if (null == ip || ip.isBlank() || unknown.equals(ip, ignoreCase = true)) {
                    ip = request.getHeader("Proxy-Client-IP")
                }
                if (null == ip || ip.isBlank() || unknown.equals(ip, ignoreCase = true)) {
                    ip = request.getHeader("WL-Proxy-Client-IP")
                }
                // natapp 内网穿透获取 IP
                if (null == ip || ip.isBlank() || unknown.equals(ip, ignoreCase = true)) {
                    ip = request.getHeader("X-Real-IP")
                }
                if (null == ip || ip.isBlank() || unknown.equals(ip, ignoreCase = true)) {
                    ip = request.getHeader("X-Natapp-Ip")
                }
                if (null == ip || ip.isBlank() || unknown.equals(ip, ignoreCase = true)) {
                    ip = request.remoteAddr
                }
            } catch (e: IllegalStateException) {
                ip = localeMessageSourceConfiguration.getMessage("message.exception.data")
            }
        }

        if (ip.equals("127.0.0.1") || ip == "0:0:0:0:0:0:0:1") {
            //根据网卡取本机配置的IP
            var inetAddress: InetAddress? = null
            try {
                inetAddress = InetAddress.getLocalHost()
            } catch (e: UnknownHostException) {
            }
            ip = inetAddress!!.hostAddress
        }
        return ip!!
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End HttpRequestUtil class

/* End of file HttpRequestUtil.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/common/util/HttpRequestUtil.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
