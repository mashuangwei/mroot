/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.system


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import java.io.Serializable

import java.math.BigInteger
import java.util.*
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern

/**
 * 请求日志实体类
 *
 * @author ErYang
 */
@TableName("system_request_log")
class RequestLog : Model<RequestLog>(), Serializable {

    companion object {

        private const val serialVersionUID = -9033827956395461023L

        /* 属性名称常量开始 */

        // -------------------------------------------------------------------------------------------------

        /**
         *表名
         */
        const val TABLE_NAME: String = "system_request_log"

        /**
         *表前缀
         */
        const val TABLE_PREFIX: String = "system_"

        /**
         * ID
         */
        const val ID: String = "id"

        /**
         * 模块
         */
        const val MODULE: String = "module"

        /**
         * 用户ID
         */
        const val USER_ID: String = "user_id"

        /**
         * 用户名
         */
        const val USERNAME: String = "username"

        /**
         * 用户类型
         */
        const val USER_TYPE: String = "user_type"

        /**
         * 客户端
         */
        const val USER_AGENT: String = "user_agent"

        /**
         * 描述
         */
        const val TITLE: String = "title"

        /**
         * 类名称
         */
        const val CLASS_NAME: String = "class_name"

        /**
         * 方法名称
         */
        const val METHOD_NAME: String = "method_name"

        /**
         * session名称
         */
        const val SESSION_NAME: String = "session_name"

        /**
         * 请求地址
         */
        const val URL: String = "url"

        /**
         * 请求的方法类型
         */
        const val METHOD_TYPE: String = "method_type"

        /**
         * 请求参数
         */
        const val PARAMS: String = "params"

        /**
         * 返回结果
         */
        const val RESULT: String = "result"

        /**
         * 执行时间,单位:毫秒
         */
        const val EXECUTE_TIME: String = "execute_time"

        /**
         * 备注
         */
        const val REMARK: String = "remark"

        /**
         * 创建时间
         */
        const val GMT_CREATE: String = "gmt_create"

        /**
         * 创建IP
         */
        const val GMT_CREATE_IP: String = "gmt_create_ip"

        /**
         * 修改时间
         */
        const val GMT_MODIFIED: String = "gmt_modified"

        // -------------------------------------------------------------------------------------------------

        /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param requestLog  RequestLog
         * @return RequestLog
         */
        fun copy2New(requestLog: RequestLog): RequestLog {
            val newRequestLog = RequestLog()
            newRequestLog.id = requestLog.id
            newRequestLog.module = requestLog.module
            newRequestLog.userId = requestLog.userId
            newRequestLog.username = requestLog.username
            newRequestLog.userType = requestLog.userType
            newRequestLog.userAgent = requestLog.userAgent
            newRequestLog.title = requestLog.title
            newRequestLog.className = requestLog.className
            newRequestLog.methodName = requestLog.methodName
            newRequestLog.sessionName = requestLog.sessionName
            newRequestLog.url = requestLog.url
            newRequestLog.methodType = requestLog.methodType
            newRequestLog.params = requestLog.params
            newRequestLog.result = requestLog.result
            newRequestLog.executeTime = requestLog.executeTime
            newRequestLog.remark = requestLog.remark
            newRequestLog.gmtCreate = requestLog.gmtCreate
            newRequestLog.gmtCreateIp = requestLog.gmtCreateIp
            newRequestLog.gmtModified = requestLog.gmtModified
            return newRequestLog
        }

        // -------------------------------------------------------------------------------------------------

    }


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    var id: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 模块
     */
    var module: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 用户ID
     */
    var userId: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 用户名
     */
    var username: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 用户类型
     */
    var userType: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 客户端
     */
    var userAgent: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 描述
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    var title: String? = null


    /**
     * 类名称
     */
    var className: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 方法名称
     */
    var methodName: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * session名称
     */
    var sessionName: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 请求地址
     */
    var url: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 请求的方法类型
     */
    var methodType: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 请求参数
     */
    var params: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 返回结果
     */
    var result: String? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 执行时间,单位:毫秒
     */
    var executeTime: Long? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 备注
     */
    @Pattern(regexp = "^[a-zA-Z0-9，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    var remark: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    var gmtCreate: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建IP
     */
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var gmtCreateIp: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    var gmtModified: Date? = null


    // -------------------------------------------------------------------------------------------------

    override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RequestLog

        if (id != other.id) return false
        if (module != other.module) return false
        if (userId != other.userId) return false
        if (username != other.username) return false
        if (userType != other.userType) return false
        if (userAgent != other.userAgent) return false
        if (title != other.title) return false
        if (className != other.className) return false
        if (methodName != other.methodName) return false
        if (sessionName != other.sessionName) return false
        if (url != other.url) return false
        if (methodType != other.methodType) return false
        if (params != other.params) return false
        if (result != other.result) return false
        if (executeTime != other.executeTime) return false
        if (remark != other.remark) return false
        if (gmtCreate != other.gmtCreate) return false
        if (gmtCreateIp != other.gmtCreateIp) return false
        if (gmtModified != other.gmtModified) return false

        return true
    }

    override fun hashCode(): Int {
        var result1 = id?.hashCode() ?: 0
        result1 = 31 * result1 + (module?.hashCode() ?: 0)
        result1 = 31 * result1 + (userId?.hashCode() ?: 0)
        result1 = 31 * result1 + (username?.hashCode() ?: 0)
        result1 = 31 * result1 + (userType?.hashCode() ?: 0)
        result1 = 31 * result1 + (userAgent?.hashCode() ?: 0)
        result1 = 31 * result1 + (title?.hashCode() ?: 0)
        result1 = 31 * result1 + (className?.hashCode() ?: 0)
        result1 = 31 * result1 + (methodName?.hashCode() ?: 0)
        result1 = 31 * result1 + (sessionName?.hashCode() ?: 0)
        result1 = 31 * result1 + (url?.hashCode() ?: 0)
        result1 = 31 * result1 + (methodType?.hashCode() ?: 0)
        result1 = 31 * result1 + (params?.hashCode() ?: 0)
        result1 = 31 * result1 + (result?.hashCode() ?: 0)
        result1 = 31 * result1 + (executeTime?.hashCode() ?: 0)
        result1 = 31 * result1 + (remark?.hashCode() ?: 0)
        result1 = 31 * result1 + (gmtCreate?.hashCode() ?: 0)
        result1 = 31 * result1 + (gmtCreateIp?.hashCode() ?: 0)
        result1 = 31 * result1 + (gmtModified?.hashCode() ?: 0)
        return result1
    }

    override fun toString(): String {
        return "RequestLog(id=$id, module=$module, userId=$userId, username=$username, userType=$userType, userAgent=$userAgent, title=$title, className=$className, methodName=$methodName, sessionName=$sessionName, url=$url, methodType=$methodType, params=$params, result=$result, executeTime=$executeTime, remark=$remark, gmtCreate=$gmtCreate, gmtCreateIp=$gmtCreateIp, gmtModified=$gmtModified)"
    }


    // -------------------------------------------------------------------------------------------------


}

// -----------------------------------------------------------------------------------------------------

// End RequestLog class

/* End of file RequestLog.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/system/RequestLog.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
