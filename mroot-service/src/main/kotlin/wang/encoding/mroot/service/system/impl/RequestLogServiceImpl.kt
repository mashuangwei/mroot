/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl


import com.baomidou.mybatisplus.mapper.SqlHelper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import wang.encoding.mroot.mapper.system.RequestLogMapper
import wang.encoding.mroot.model.entity.system.RequestLog
import wang.encoding.mroot.service.system.RequestLogService
import java.math.BigInteger
import java.time.Instant
import java.util.*


/**
 * 后台 请求日志 Service 接口实现类
 *
 * @author ErYang
 */
@Service
class RequestLogServiceImpl : BaseServiceImpl
<RequestLogMapper, RequestLog>(), RequestLogService {

    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(RequestLogServiceImpl::class.java)
    }


    /**
     * 初始化新增 RequestLog 对象
     *
     * @param requestLog RequestLog
     * @return RequestLog
     */
    override fun initSaveRequestLog(requestLog: RequestLog): RequestLog {
        requestLog.gmtCreate = Date.from(Instant.now())
        return requestLog
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    override fun validationRequestLog(requestLog: RequestLog): String? {
        return HibernateValidationUtil.validateEntity(requestLog)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 请求日志
     *
     * @param requestLog RequestLog
     * @return ID  BigInteger
     */
    override fun saveBackId(requestLog: RequestLog): BigInteger? {
        val id: Int? = super.save(requestLog)
        if (null != id && 0 < id) {
            return requestLog.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清空记录
     */
    override fun truncate(): Boolean {
        return SqlHelper.retBool(superMapper!!.truncate())
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 定时任务删除
     *
     * @param size Int 数量
     */
    override fun remove2QuartzJob(size: Int): Boolean {
        val params = HashMap<String, Int>()
        params["size"] = size
        return SqlHelper.retBool(superMapper!!.remove2QuartzJob(params))
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RequestLogServiceImpl class

/* End of file RequestLogServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/system/impl/RequestLogServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
