/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import wang.encoding.mroot.mapper.system.RoleMapper
import wang.encoding.mroot.model.entity.system.Role
import wang.encoding.mroot.model.enums.StatusEnum
import wang.encoding.mroot.service.system.RoleService
import java.math.BigInteger
import java.time.Instant
import java.util.*
import kotlin.collections.ArrayList


/**
 * 后台 角色 Service 接口实现类
 *
 * @author ErYang
 */
@Service
class RoleServiceImpl : BaseServiceImpl
<RoleMapper, Role>(), RoleService {


    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(RoleServiceImpl::class.java)
        /**
         * 逗号
         */
        private const val COMMA_NAME: String = ","
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化新增 Role 对象
     *
     * @param role Role
     * @return Role
     */
    override fun initSaveRole(role: Role): Role {
        role.status = StatusEnum.NORMAL.key
        role.gmtCreate = Date.from(Instant.now())
        return role
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 Role 对象
     *
     * @param role Role
     * @return Role
     */
    override fun initEditRole(role: Role): Role {
        role.gmtModified = Date.from(Instant.now())
        return role
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    override fun validationRole(role: Role): String? {
        return HibernateValidationUtil.validateEntity(role)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 角色
     *
     * @param role Role
     * @return ID  BigInteger
     */
    override fun saveBackId(role: Role): BigInteger? {
        val id: Int? = super.save(role)
        if (null != id && 0 < id) {
            return role.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 角色(更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun removeBackId(id: BigInteger): BigInteger? {
        val role = Role()
        role.id = id
        role.status = StatusEnum.DELETE.key
        role.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.remove2StatusById(role)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 角色 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun recoverBackId(id: BigInteger): BigInteger? {
        val role = Role()
        role.id = id
        role.status = StatusEnum.NORMAL.key
        role.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.recover2StatusById(role)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 角色
     *
     * @param role Role
     * @return ID  BigInteger
     */
    override fun updateBackId(role: Role): BigInteger? {
        val flag: Boolean = super.updateById(role)
        if (flag) {
            return role.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 role
     *
     * @param name 标识
     * @return Role
     */
    override fun getByName(name: String): Role? {
        val role = Role()
        role.name = name
        return super.getByModel(role)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 role
     *
     * @param title 名称
     * @return Role
     */
    override fun getByTitle(title: String): Role? {
        val role = Role()
        role.title = title
        return super.getByModel(role)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户 id 查询角色
     *
     * @param userId 用户id
     * @return Set<Role>
     */
    override fun listByUserId(userId: BigInteger): Set<Role>? {
        return superMapper?.listByUserId(userId)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户 id 查询角色
     *
     * @param userId 用户id
     * @return Role
     */
    override fun getByUserId(userId: BigInteger): Role? {
        return superMapper?.getByUserId(userId)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据用户 id 查询角色 id
     *
     * @param userId 用户id
     * @return id 集合字符串
     */
    override fun listIdByUserId2String(userId: BigInteger): String? {
        val list: Set<Role>? = superMapper?.listByUserId(userId)
        if (null != list && list.isNotEmpty()) {
            val stringBuffer = StringBuilder()
            stringBuffer.append("")
            val listSize: Int = list.size
            for ((i: Int, role: Role) in list.withIndex()) {
                stringBuffer.append(role.id)
                if (i != listSize - 1) {
                    stringBuffer.append(COMMA_NAME)
                }
            }
            return stringBuffer.toString()
        }
        return null

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量新增 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleIdArray Array<BigInteger>
     *
     * @return Int
     */
    override fun saveBatchByUserIdAndRoleIdArray(userId: BigInteger, roleIdArray: Array<BigInteger>): Int? {
        val params = HashMap<String, Any>()
        params["userId"] = userId
        params["roleIdArray"] = roleIdArray
        return superMapper?.saveBatchByUserIdAndRoleIdArray(params)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户 id 和 角色 id 批量删除 用户-角色 表
     *
     * @param userId BigInteger
     * @param roleIdArray Array<BigInteger>
     *
     * @return Int
     */
    override fun removeBatchByUserIdAndRoleIdArray(userId: BigInteger, roleIdArray: Array<BigInteger>?): Int? {
        val params = HashMap<String, Any>()
        params["userId"] = userId
        if (null != roleIdArray && roleIdArray.isNotEmpty()) {
            params["roleIdArray"] = roleIdArray
        }
        return superMapper?.removeBatchByUserIdAndRoleIdArray(params)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户id 删除 用户-角色 表
     *
     * @param userId BigInteger
     *
     * @return Int
     */
    override fun removeByUserId(userId: BigInteger): Int? {
        return superMapper?.removeByUserId(userId)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户id 删除 删除 用户-角色 表
     *
     * @param userIdArray  Array<BigInteger>
     *
     * @return Int
     */
    override fun removeByUserIdArray(userIdArray: ArrayList<BigInteger>?): Int? {
        val params = HashMap<String, Any>()
        if (null != userIdArray && userIdArray.isNotEmpty()) {
            params["userIdArray"] = userIdArray
        }
        return superMapper?.removeByUserIdArray(params)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RoleServiceImpl class

/* End of file RoleServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/system/impl/RoleServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
