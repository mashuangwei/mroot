/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import wang.encoding.mroot.model.entity.system.ScheduleJob
import wang.encoding.mroot.model.enums.StatusEnum
import org.springframework.stereotype.Service
import wang.encoding.mroot.mapper.system.ScheduleJobMapper
import wang.encoding.mroot.service.system.ScheduleJobService

import java.math.BigInteger
import java.time.Instant
import java.util.Date


/**
 * 后台 定时任务 Service 接口实现类
 *
 * @author ErYang
 */
@Service
class ScheduleJobServiceImpl : BaseServiceImpl
<ScheduleJobMapper, ScheduleJob>(), ScheduleJobService {

    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(ScheduleJobServiceImpl::class.java)
    }

    /**
     * 初始化新增 ScheduleJob 对象
     *
     * @param scheduleJob ScheduleJob
     * @return ScheduleJob
     */
    override fun initSaveScheduleJob(scheduleJob: ScheduleJob): ScheduleJob {
        scheduleJob.status = StatusEnum.NORMAL.key
        scheduleJob.gmtCreate = Date.from(Instant.now())
        return scheduleJob
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 ScheduleJob 对象
     *
     * @param scheduleJob ScheduleJob
     * @return ScheduleJob
     */
    override fun initEditScheduleJob(scheduleJob: ScheduleJob): ScheduleJob {
        scheduleJob.gmtModified = Date.from(Instant.now())
        return scheduleJob
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    override fun validationScheduleJob(scheduleJob: ScheduleJob): String? {
        return HibernateValidationUtil.validateEntity(scheduleJob)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 scheduleJob
     *
     * @param name 标识
     * @return ScheduleJob
     */
    override fun getByName(name: String): ScheduleJob? {
        val scheduleJob = ScheduleJob()
        scheduleJob.name = name
        return super.getByModel(scheduleJob)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 scheduleJob
     *
     * @param title 名称
     * @return ScheduleJob
     */
    override fun getByTitle(title: String): ScheduleJob? {
        val scheduleJob = ScheduleJob()
        scheduleJob.title = title
        return super.getByModel(scheduleJob)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 定时任务
     *
     * @param scheduleJob ScheduleJob
     * @return ID  BigInteger
     */
    override fun saveBackId(scheduleJob: ScheduleJob): BigInteger? {
        val id: Int? = super.save(scheduleJob)
        if (null != id && 0 < id) {
            return scheduleJob.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 定时任务(更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun removeBackId(id: BigInteger): BigInteger? {
        val scheduleJob = ScheduleJob()
        scheduleJob.id = id
        scheduleJob.status = StatusEnum.DELETE.key
        scheduleJob.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.remove2StatusById(scheduleJob)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 定时任务
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun removeByIdBackId(id: BigInteger): BigInteger? {
        val scheduleJob = ScheduleJob()
        scheduleJob.id = id
        val backId: Int? = super.removeById(id)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 定时任务
     *
     * @param scheduleJob ScheduleJob
     * @return ID  BigInteger
     */
    override fun updateBackId(scheduleJob: ScheduleJob): BigInteger? {
        val flag: Boolean = super.updateById(scheduleJob)
        if (flag) {
            return scheduleJob.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobServiceImpl class

/* End of file ScheduleJobServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/system/impl/ScheduleJobServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
